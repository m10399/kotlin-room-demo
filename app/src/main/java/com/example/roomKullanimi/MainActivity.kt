package com.example.roomKullanimi

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch


class MainActivity : AppCompatActivity() {
    private lateinit var vt:Veritabani
    private lateinit var kdao:KisilerDao
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        vt = Veritabani.veritabaniErisim(this)!!
        kdao = vt.getKisilerDao()

        //Ekle()
        //rastgele()
        //kisileriYukle()
        //Ara()
        //Getir()
        Kontrol()
    }
    fun kisileriYukle(){
        val job = CoroutineScope(Dispatchers.Main).launch {
            val gelenListe = kdao.tumKisiler()
            for (k in gelenListe){
                Log.e("Kişi id",k.kisi_id.toString())
                Log.e("Kişi ad",k.kisi_ad)
                Log.e("Kişi yaş",k.kisi_yas.toString())

            }
        }
    }
    fun Ekle(){
        val job = CoroutineScope(Dispatchers.Main).launch {
            val yeniKisi= Kisiler(0, kisi_ad = "Çağlar",25)
            kdao.KisiEkle(yeniKisi)
        }
    }
    fun Guncelle(){
        val job = CoroutineScope(Dispatchers.Main).launch {
            val guncelleKisi= Kisiler(2, kisi_ad = "Ahmet",180)
            kdao.KisiGuncelle(guncelleKisi)
        }
    }

    fun Sil(){
        val job = CoroutineScope(Dispatchers.Main).launch {
            val kisisilinecek= Kisiler(1, kisi_ad = "Ahmet",180)
            kdao.KisiSil(kisisilinecek)
        }
    }

    fun rastgele(){
        val job = CoroutineScope(Dispatchers.Main).launch {
            val gelenListe = kdao.rastgele()
            for (k in gelenListe){
                Log.e("Kişi id",k.kisi_id.toString())
                Log.e("Kişi ad",k.kisi_ad)
                Log.e("Kişi yaş",k.kisi_yas.toString())

            }
        }
    }

    fun Ara(){
        val job = CoroutineScope(Dispatchers.Main).launch {
            val gelenListe = kdao.kisiAra("e")
            for (k in gelenListe){
                Log.e("Kişi id",k.kisi_id.toString())
                Log.e("Kişi ad",k.kisi_ad)
                Log.e("Kişi yaş",k.kisi_yas.toString())

            }
        }
    }

    fun Getir(){
        val job = CoroutineScope(Dispatchers.Main).launch {
            val gelenListe = kdao.kisiGetir(3)

            Log.e("Kişi id",gelenListe.kisi_id.toString())
            Log.e("Kişi ad",gelenListe.kisi_ad)
            Log.e("Kişi yaş",gelenListe.kisi_yas.toString())


        }
    }

    fun Kontrol(){
        val job = CoroutineScope(Dispatchers.Main).launch {
            val gelenSonuc = kdao.kayitKontrol("Remzi")

            Log.e("Kişi kontrol sonucu",gelenSonuc.toString())


        }
    }
}