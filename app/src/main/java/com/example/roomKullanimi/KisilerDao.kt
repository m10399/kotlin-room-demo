package com.example.roomKullanimi

import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update

@Dao
interface KisilerDao {
    @Query("Select * from kisiler")
    suspend fun tumKisiler() : List<Kisiler>

    @Insert
    suspend fun KisiEkle(kisi:Kisiler)

    @Update
    suspend fun KisiGuncelle(kisi:Kisiler)

    @Delete
    suspend fun KisiSil(kisi:Kisiler)

    @Query("Select * from kisiler ORDER BY RANDOM() LIMIT 1")
    suspend fun rastgele() : List<Kisiler>

    @Query("Select * from kisiler where kisi_ad like '%' || :aramaKelimesi || '%'")
    suspend fun kisiAra(aramaKelimesi:String) : List<Kisiler>

    @Query("Select * from kisiler where kisi_id = :kisi_id")
    suspend fun kisiGetir(kisi_id:Int) : Kisiler

    @Query("Select count(*) from kisiler where kisi_ad = :kisi_ad")
    suspend fun kayitKontrol(kisi_ad:String) : Int
}